package com.example.weatherapp;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class WeatherViewModel extends AndroidViewModel {
    private WeatherRepository weatherRepository;
    private LiveData<List<Weather>> weathers;
    public WeatherViewModel(@NonNull Application application){
        super(application);
        weatherRepository = new WeatherRepository(application);
        weathers = weatherRepository.findAllEntries();
    }
    public LiveData<List<Weather>> findAll(){
        return weathers;
    }
    public void insert(Weather weather){
        weatherRepository.insert(weather);
    }
    public void update(Weather weather){
        weatherRepository.update(weather);
    }
    public void delete(Weather weather){
        weatherRepository.delete(weather);
    }
}
