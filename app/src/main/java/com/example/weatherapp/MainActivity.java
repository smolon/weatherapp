package com.example.weatherapp;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;

import com.example.weatherapp.ui.home.HomeFragment;
import com.example.weatherapp.ui.home.HomeViewModel;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import android.os.StrictMode;
import android.util.Log;
import android.view.View;

import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.widget.ImageView;
import android.widget.TextView;

import net.aksingh.owmjapis.api.APIException;
import net.aksingh.owmjapis.core.OWM;
import net.aksingh.owmjapis.model.CurrentWeather;
import net.aksingh.owmjapis.model.param.Main;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.LinkedList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;
    private Toolbar toolbar;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private FusedLocationProviderClient fusedLocationClient;
    private TextView weatherDesc;
    private TextView localisation;
    private ImageView weatherIcon;
    private Location loc;
    private List<KindsOfWeather> defaultWeathers;
    private TextView weatherName;
    private String weatherDescString;
    private String localisationString;
    private int weatherIconstring;
    private String weatherNameString;
    public String getWeatherDescString(){
        return weatherDescString;
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Log.d("lokacja", "onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        //Log.d("lokacja", "onPause");
    }

    public String getLocalisationString() {
        return localisationString;
    }

    public int getWeatherIconstring() {
        return weatherIconstring;
    }

    public String getWeatherNameString() {
        return weatherNameString;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        //Log.d("lokacja", "no odpalilo sie oncreate");
        defaultWeathers = new LinkedList<KindsOfWeather>();
        defaultWeathers.add(new KindsOfWeather("Thunderstorm", "11d"));
        defaultWeathers.add(new KindsOfWeather("Drizzle", "09d"));
        defaultWeathers.add(new KindsOfWeather("Rain", "10d"));
        defaultWeathers.add(new KindsOfWeather("Snow", "13d"));
        defaultWeathers.add(new KindsOfWeather("Mist", "50d"));
        defaultWeathers.add(new KindsOfWeather("Fog", "50d"));
        defaultWeathers.add(new KindsOfWeather("Clear", "01d"));
        defaultWeathers.add(new KindsOfWeather("Clouds", "02d"));
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        /*FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                //overridePendingTransition(0, 0);
                startActivity(getIntent());
                //overridePendingTransition(0, 0);
                *//*Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*//*
            }
        });*/
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<android.location.Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        weatherDesc = findViewById(R.id.weather_description);
                        localisation = findViewById(R.id.localisation_name);
                        weatherIcon = findViewById(R.id.weather_icon);
                        weatherName = findViewById(R.id.weather_name);
                        Bundle bundle = new Bundle();
                        // Got last known location. In some rare situations this can be null.

                        if(location==null){

                        }
                        if (location != null) {
                            // Logic to handle location object
                            try{
                                // declaring object of "OWM" class
                                OWM owm = new OWM("8284518e04e2fd4601791db0b75e8648");

                                // getting current weather data
                                CurrentWeather cwd1 = owm.currentWeatherByCoords(location.getLatitude(),location.getLongitude());
                                double temp = cwd1.getMainData().getTemp()-273.15;
                                double roundTemp = round(temp, 1);
                                String mainWet = cwd1.getWeatherList().get(0).getMainInfo();
                                int resId;
                                for(int i=0; i<defaultWeathers.size();i++){
                                    if(defaultWeathers.get(i).getMain().equalsIgnoreCase(mainWet)){
                                        String iconName = "icon" + defaultWeathers.get(i).getIcon();
                                        resId = MainActivity.this.getResources().getIdentifier(
                                                iconName,
                                                "drawable",
                                                MainActivity.this.getPackageName()
                                        );
                                        weatherIcon.setImageResource(resId);
                                        bundle.putInt("weatherIcon", resId);
                                        weatherIconstring = resId;
                                    }
                                }
                                weatherDesc.setText(String.valueOf(roundTemp) + " °C");
                                localisation.setText(cwd1.getCityName());
                                weatherName.setText(cwd1.getWeatherList().get(0).getDescription());

                                HomeFragment homeFragment = new HomeFragment();
                                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                                bundle.putString("weatherDesc", String.valueOf(roundTemp));
                                bundle.putString("localisation", cwd1.getCityName());
                                bundle.putString("weatherName",cwd1.getWeatherList().get(0).getDescription());
                                weatherDescString = String.valueOf(roundTemp);
                                localisationString = cwd1.getCityName();
                                weatherNameString = cwd1.getWeatherList().get(0).getDescription();
                                homeFragment.setArguments(bundle);
                                HomeViewModel homeViewModel = new HomeViewModel();
                                homeViewModel.setText(localisationString);
                                homeFragment.setLocalisation(cwd1.getCityName());
                                fragmentTransaction.addToBackStack(null);
                                fragmentTransaction.replace(R.id.fragment_container_view_tag,homeFragment);

                            }catch(APIException e){
                                //Log.d("lokacja", "ta no jakis blad");
                            }
                        }
                    }
                });
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow,
                R.id.nav_tools, R.id.nav_share, R.id.nav_send)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }
    public double round(double value, int places){
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(Double.toString(value));
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
