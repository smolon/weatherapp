package com.example.weatherapp;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface WeatherDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Weather weather);

    @Update
    public void update(Weather weather);

    @Delete
    public void delete(Weather weather);

    @Query("DELETE FROM weather")
    public void deleteAll();

    @Query("SELECT * FROM weather ORDER BY city")
    public LiveData<List<Weather>> findAll();

    @Query("SELECT * FROM weather WHERE city LIKE :city")
    public List<Weather> findWeatherInCity(String city);
}
