package com.example.weatherapp;

public class CurrentWeatherData {
    private String weatherTemp;
    private String weatherDescription;
    private String weatherLocation;
    private int weatherIcon;

    public void setWeatherTemp(String weatherTemp) {
        this.weatherTemp = weatherTemp;
    }

    public void setWeatherDescription(String weatherDescription) {
        this.weatherDescription = weatherDescription;
    }

    public void setWeatherLocation(String weatherLocation) {
        this.weatherLocation = weatherLocation;
    }

    public void setWeatherIcon(int weatherIcon) {
        this.weatherIcon = weatherIcon;
    }

    public String getWeatherTemp() {
        return weatherTemp;
    }

    public String getWeatherDescription() {
        return weatherDescription;
    }

    public String getWeatherLocation() {
        return weatherLocation;
    }

    public int getWeatherIcon() {
        return weatherIcon;
    }
}
