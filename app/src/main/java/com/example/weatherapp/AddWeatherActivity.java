package com.example.weatherapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class AddWeatherActivity extends AppCompatActivity {
    public static final String EXTRA_ADD_WEATHER_CITY = "ADD_WEATHER_CITY";
    private EditText editCity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_weather);
        editCity = findViewById(R.id.add_city_edittext);
        final Button button = findViewById(R.id.add_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent replyIntent = new Intent();
                if(TextUtils.isEmpty(editCity.getText())){
                    setResult(RESULT_CANCELED, replyIntent);
                }
                else {
                    String city = editCity.getText().toString();
                    replyIntent.putExtra(EXTRA_ADD_WEATHER_CITY, city);
                    setResult(RESULT_OK,replyIntent);
                }
                finish();
            }
        });
    }
}
