package com.example.weatherapp;

import android.app.Application;

import androidx.lifecycle.LiveData;

import java.util.List;

public class WeatherRepository {
    private WeatherDao weatherDao;
    private LiveData<List<Weather>> weathers;
    WeatherRepository(Application application){
        WeatherDatabase database = WeatherDatabase.getDatabase(application);
        weatherDao = database.weatherDao();
        weathers = weatherDao.findAll();
    }
    LiveData<List<Weather>> findAllEntries(){
        return weathers;
    }
    void insert(Weather weather){
        WeatherDatabase.databaseWriteExecutor.execute(()->{
            weatherDao.insert(weather);
        });
    }
    void update(Weather weather){
        WeatherDatabase.databaseWriteExecutor.execute(()->{
            weatherDao.update(weather);
        });
    }
    void delete(Weather weather){
        WeatherDatabase.databaseWriteExecutor.execute(()->{
            weatherDao.delete(weather);
        });
    }
}
