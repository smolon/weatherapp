package com.example.weatherapp.ui.home;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.weatherapp.CurrentWeatherData;
import com.example.weatherapp.MainActivity;

public class HomeViewModel extends ViewModel {

    private MutableLiveData<String> mText;
    private MutableLiveData<String> mLocalisation;
    private MutableLiveData<CurrentWeatherData> currentWeatherDataMutableLiveData;

    public HomeViewModel() {
        mLocalisation = new MutableLiveData<>();
        MainActivity mainActivity = new MainActivity();
        //mLocalisation.setValue(mainActivity.getLocalisationString());
        //Log.d("lokacja", "mLocalisation.setValue(mainActivity.getLocalisationString()); :  " + mainActivity.getLocalisationString());
        mText = new MutableLiveData<>();
        mText.setValue("This is home fragment");
    }

    public LiveData<String> getText() {
        return mLocalisation;
    }
    public void setText(String text){
        mLocalisation.setValue(text);
        Log.d("lokacja", "mLocalisation z viewmodel po metodzie setetxt" + getText());
    }
}