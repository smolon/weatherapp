package com.example.weatherapp.ui.gallery;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.weatherapp.AddWeatherActivity;
import com.example.weatherapp.R;
import com.example.weatherapp.Weather;
import com.example.weatherapp.WeatherInfo;
import com.example.weatherapp.WeatherViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import org.w3c.dom.Text;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.LinkedList;
import java.util.List;

import static android.app.Activity.RESULT_OK;

public class GalleryFragment extends Fragment {

    public static final int NEW_WEATHER_REQUEST_CODE = 1;
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == NEW_WEATHER_REQUEST_CODE && resultCode == RESULT_OK){
            Weather weather = new Weather(data.getStringExtra(AddWeatherActivity.EXTRA_ADD_WEATHER_CITY), 0);
            Log.d("lokacja",data.getStringExtra(AddWeatherActivity.EXTRA_ADD_WEATHER_CITY));
            Log.d("lokacja", "weather: "+  weather.getCity());
            weatherViewModel.insert(weather);
            AlertDialog.Builder b = new AlertDialog.Builder(getContext());
            b.setMessage("Ostatnia temperatura obecnie pokazuje 0, zmieni się to po kliknięciu na daną pozycję");
            b.setCancelable(false);
            b.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            b.setTitle("Dodano pozycję");
            AlertDialog ad = b.create();
            ad.show();
        }
        else{
            AlertDialog.Builder b = new AlertDialog.Builder(getContext());
            b.setMessage("Nie udało się dodać pozyzji");
            b.setCancelable(false);
            b.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            b.setTitle("Operacja nie powiodła się");
            AlertDialog ad = b.create();
            ad.show();
        }
    }

    private GalleryViewModel galleryViewModel;
    private WeatherViewModel weatherViewModel;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        galleryViewModel =
                ViewModelProviders.of(this).get(GalleryViewModel.class);
        final View root = inflater.inflate(R.layout.fragment_gallery, container, false);
        //final LinkedList<WeatherInfo> weatherInfos = WeatherInfo.createSampleList(20);
        //final TextView textView = root.findViewById(R.id.text_gallery);
        FloatingActionButton addPositionButton = root.findViewById(R.id.fab_add);
        addPositionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),AddWeatherActivity.class);
                startActivityForResult(intent,NEW_WEATHER_REQUEST_CODE);
            }
        });
        galleryViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                //textView.setText(s);
                RecyclerView rvWeathers = (RecyclerView) root.findViewById(R.id.list_of_locations);
                DividerItemDecoration itemDecoration = new DividerItemDecoration(getContext(), DividerItemDecoration.HORIZONTAL);
                rvWeathers.addItemDecoration(itemDecoration);
                WeatherAdapter adapter = new WeatherAdapter();
                rvWeathers.setAdapter(adapter);
                rvWeathers.setLayoutManager(new LinearLayoutManager(getContext()));
                weatherViewModel = ViewModelProviders.of(getActivity()).get(WeatherViewModel.class);
                weatherViewModel.findAll().observe(getActivity(), new Observer<List<Weather>>() {
                    @Override
                    public void onChanged(List<Weather> weathers) {
                        adapter.setWeathers(weathers);
                    }
                });
            }
        });
        return root;
    }
    private class WeatherHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener, View.OnClickListener{
        private TextView CityName;
        private TextView lastTemp;
        private Weather weather2;
        public WeatherHolder(LayoutInflater inflater, ViewGroup parent){
            super(inflater.inflate(R.layout.single_location,parent, false));
            itemView.setOnClickListener(this::onClick);
            itemView.setOnLongClickListener(this::onLongClick);
            CityName = itemView.findViewById(R.id.city_name);
            lastTemp = itemView.findViewById(R.id.last_temp);
        }
        public void bind (Weather weather){
            weather2 = weather;
            CityName.setText(weather.getCity());
            lastTemp.setText("Last temperature: " + round(weather.getTemp(),1)+ " °C");
        }
        @Override
        public void onClick(View v) {
            /*editedBook = book;

            Intent editIntent = new Intent(MainActivity.this, EditBookActivity.class);

            editIntent.putExtra(EditBookActivity.EXTRA_EDIT_BOOK_TITLE, titleTextView.getText().toString());
            editIntent.putExtra(EditBookActivity.EXTRA_EDIT_BOOK_AUTHOR, authorTextView.getText().toString());

            startActivityForResult(editIntent, EDIT_BOOK_ACTIVITY_REQUEST_CODE);*/
        }
        @Override
        public boolean onLongClick(View v) {
            android.app.AlertDialog.Builder builder1 = new android.app.AlertDialog.Builder(getContext());
            builder1.setMessage("Are you sure?");
            builder1.setCancelable(true);
            builder1.setTitle("Delete position");

            builder1.setPositiveButton(
                    "Yes",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            weatherViewModel.delete(weather2);
                            dialog.cancel();
                        }
                    });

            builder1.setNegativeButton(
                    "No",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });

            android.app.AlertDialog alert11 = builder1.create();
            alert11.show();
            return false;
        }
    }
    private class WeatherAdapter extends RecyclerView.Adapter<WeatherHolder>{
        private List<Weather> weathers;
        /*WeatherAdapter(LinkedList<Weather> wwwww){
            weathers = wwwww;
        }*/
        @NonNull
        @Override
        public WeatherHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType){
            return new WeatherHolder(getLayoutInflater(), parent);
        }
        @Override
        public void onBindViewHolder(@NonNull WeatherHolder holder, int position){
            if(weathers!=null){
                Weather weather = weathers.get(position);
                holder.bind(weather);
            }
            else{
                Log.d("lokacja", "brak książek");
            }
        }
        @Override
        public int getItemCount(){
            if(weathers !=null){
                return weathers.size();
            }
            else{
                return 0;
            }
        }
        void setWeathers(List<Weather> weathers){
            this.weathers = weathers;
            notifyDataSetChanged();
        }

    }
    public double round(double value, int places){
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(Double.toString(value));
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}