package com.example.weatherapp.ui.home;

import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import net.aksingh.owmjapis.api.APIException;
import net.aksingh.owmjapis.core.OWM;
import net.aksingh.owmjapis.model.CurrentWeather;
import net.aksingh.owmjapis.model.param.Main;

import com.example.weatherapp.MainActivity;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import com.example.weatherapp.KindsOfWeather;
import com.example.weatherapp.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.LinkedList;
import java.util.List;

public class HomeFragment extends Fragment {

    public static final int NEW_WEATHER_REQUEST_CODE = 1;
    private HomeViewModel homeViewModel;
    private Toolbar toolbar;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private FusedLocationProviderClient fusedLocationClient;
    private TextView weatherDesc;
    private TextView localisation;
    private ImageView weatherIcon;
    private Location loc;
    private List<KindsOfWeather> defaultWeathers;
    private TextView weatherName;
    private String weatherDescString;
    private String localisationString;
    private int weatherIconstring;
    private String weatherNameString;




    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel.class);

        View root = inflater.inflate(R.layout.fragment_home, container, false);
        defaultWeathers = new LinkedList<KindsOfWeather>();
        defaultWeathers.add(new KindsOfWeather("Thunderstorm", "11d"));
        defaultWeathers.add(new KindsOfWeather("Drizzle", "09d"));
        defaultWeathers.add(new KindsOfWeather("Rain", "10d"));
        defaultWeathers.add(new KindsOfWeather("Snow", "13d"));
        defaultWeathers.add(new KindsOfWeather("Mist", "50d"));
        defaultWeathers.add(new KindsOfWeather("Fog", "50d"));
        defaultWeathers.add(new KindsOfWeather("Clear", "01d"));
        defaultWeathers.add(new KindsOfWeather("Clouds", "02d"));
        weatherDesc = root.findViewById(R.id.weather_description);
        localisation = root.findViewById(R.id.localisation_name);
        weatherIcon = root.findViewById(R.id.weather_icon);
        weatherName = root.findViewById(R.id.weather_name);
        /*FloatingActionButton fab = root.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment currentFragment = getActivity().getSupportFragmentManager().findFragmentById(R.id.fragment_container_view_tag);
                if (currentFragment instanceof HomeFragment) {
                    FragmentTransaction fragTransaction =   getActivity().getSupportFragmentManager().beginTransaction();
                    fragTransaction.detach(currentFragment);
                    fragTransaction.attach(currentFragment);
                    fragTransaction.commit();
                    Log.d("lokacja", "guzik w homefragment");
                }
            }
        });*/
        //root.findViewById()

        Bundle bundle = getArguments();
        MainActivity mainActivity = new MainActivity();
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(getActivity(), new OnSuccessListener<android.location.Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        /*weatherDesc = findViewById(R.id.weather_description);
                        localisation = findViewById(R.id.localisation_name);
                        weatherIcon = findViewById(R.id.weather_icon);
                        weatherName = findViewById(R.id.weather_name);*/
                        Bundle bundle = new Bundle();
                        // Got last known location. In some rare situations this can be null.

                        /*weatherDesc = findViewById(R.id.weather_description);
                        localisation = findViewById(R.id.localisation_name);
                        weatherIcon = findViewById(R.id.weather_icon);
                        weatherName = findViewById(R.id.weather_name);*/
                        if(location==null){
                            //Log.d("lokacja", "nie pobrano aktuylanych wspolrzednych");
                            //weatherDesc.setText("Nie dziala cos");
                            //localisation.setText("no nie dziala");
                            AlertDialog.Builder b = new AlertDialog.Builder(getContext());
                            b.setMessage("Nie udało się pobrać lokalizacji");
                            b.setCancelable(false);
                            b.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            b.setTitle("Błąd");
                            AlertDialog ad = b.create();
                            ad.show();
                        }
                        if (location != null) {
                            // Logic to handle location object
                            try{
                                // declaring object of "OWM" class
                                OWM owm = new OWM("8284518e04e2fd4601791db0b75e8648");

                                // getting current weather data for the "London" city
                                CurrentWeather cwd1 = owm.currentWeatherByCoords(location.getLatitude(),location.getLongitude());
                                double temp = cwd1.getMainData().getTemp()-273.15;
                                double roundTemp = round(temp, 1);
                                String mainWet = cwd1.getWeatherList().get(0).getMainInfo();
                                int resId;
                                for(int i=0; i<defaultWeathers.size();i++){
                                    if(defaultWeathers.get(i).getMain().equalsIgnoreCase(mainWet)){
                                        String iconName = "icon" + defaultWeathers.get(i).getIcon();
                                        resId = getActivity().getResources().getIdentifier(
                                                iconName,
                                                "drawable",
                                                getActivity().getPackageName()
                                        );
                                        weatherIcon.setImageResource(resId);
                                        bundle.putInt("weatherIcon", resId);
                                        weatherIconstring = resId;
                                    }
                                }
                                weatherDesc.setText(String.valueOf(roundTemp) + " °C");
                                localisation.setText(cwd1.getCityName());
                                weatherName.setText(cwd1.getWeatherList().get(0).getDescription());

                                HomeFragment homeFragment = new HomeFragment();
                                bundle.putString("weatherDesc", String.valueOf(roundTemp));
                                bundle.putString("localisation", cwd1.getCityName());
                                bundle.putString("weatherName",cwd1.getWeatherList().get(0).getDescription());
                                weatherDescString = String.valueOf(roundTemp);
                                localisationString = cwd1.getCityName();
                                weatherNameString = cwd1.getWeatherList().get(0).getDescription();
                                homeFragment.setArguments(bundle);

                            }catch(APIException e){
                                Log.d("lokacja", "ta no jakis blad");
                            }
                        }
                    }
                });
                final TextView textView = root.findViewById(R.id.weather_name);
        homeViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                //localisation.setText(s);
                weatherDesc.setText(weatherDescString);
                localisation.setText(localisationString);
                weatherName.setText(weatherNameString);
                weatherIcon.setImageResource(weatherIconstring);
            }
        });
        return root;
    }
   /* public void setValues(String weatherDesc, String localisation, String weatherIcon, String weatherName){
        this.weatherDesc = root.findViewById(R.id.weather_description);
        this.localisation = root.findViewById(R.id.localisation_name);
        this.weatherIcon = root.findViewById(R.id.weather_icon);
        this.weatherName = root.findViewById(R.id.weather_name);
    }*/
   public void setLocalisation(String localisation){
       this.localisationString = localisation;
   }
    public double round(double value, int places){
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(Double.toString(value));
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}