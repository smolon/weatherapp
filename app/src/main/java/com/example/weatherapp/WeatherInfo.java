package com.example.weatherapp;

import java.util.LinkedList;

public class WeatherInfo {
    private String CityName;
    private String LastTemp;
    public WeatherInfo(String CityName, String LastTemp){
        this.CityName = CityName;
        this.LastTemp = LastTemp;
    }
    public String getCityName(){
        return CityName;
    }
    public String getLastTemp(){
        return LastTemp;
    }
    public static LinkedList<WeatherInfo> createSampleList(int howMany){
        LinkedList<WeatherInfo> weatherInfos = new LinkedList<>();
        for(int i=0;i<howMany;i++){
            weatherInfos.add(new WeatherInfo("Miasto: "+i, String.valueOf(i*1.5)));
        }
        return weatherInfos;
    }
}
