package com.example.weatherapp;

import com.google.gson.annotations.SerializedName;

public class Location {
    @SerializedName("name")
    private String CityName;
    @SerializedName("country")
    private String country;
    @SerializedName("dt")
    private double epoch;
    @SerializedName("lon")
    private double longitude;
    @SerializedName("lat")
    private double latitude;
    @SerializedName("overcast clouds")
    private String description;
    public Location(String CityName, String country, double epoch, double longitude, double latitude, String description){
        this.CityName = CityName;
        this.country = country;
        this.epoch = epoch;
        this.longitude = longitude;
        this.latitude = latitude;
        this.description = description;
    }
    public String getCityName() {
        return CityName;
    }

    public String getCountry() {
        return country;
    }

    public double getEpoch() {
        return epoch;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public String getDescription() {
        return description;
    }

    public void setCityName(String cityName) {
        CityName = cityName;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setEpoch(double epoch) {
        this.epoch = epoch;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
